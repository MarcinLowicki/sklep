package sg.sklep;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class Kredyt extends HttpServlet {
    private static BigDecimal minKwotaPozostawionaNaOsobe = new BigDecimal("600.5");

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("kredyt.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ArrayList<String> kredytyLi = new ArrayList<String>();
        Integer dochod = Integer.parseInt(req.getParameter("dochod"));
        BigDecimal kwotaZakupu = new BigDecimal(req.getParameter("kwota"));
        BigDecimal kosztUtrzym = new BigDecimal(req.getParameter("kutrzym"));
        String kodPocz = req.getParameter("kodp");

        if (req.getSession().getAttribute("kredytyList") != null)
            kredytyLi = (ArrayList<String>) req.getSession().getAttribute("kredytyList");


        BigDecimal wolneMiesSrodki = new BigDecimal(dochod.toString()).subtract(kosztUtrzym);
        BigDecimal ileZostajeNaSplate = wolneMiesSrodki.subtract(minKwotaPozostawionaNaOsobe.multiply(new BigDecimal(Integer.parseInt(req.getParameter("osobyd")))));
        ileZostajeNaSplate = ileZostajeNaSplate.subtract(new BigDecimal(kodPocz.charAt(1))); //odejmuje drugą cyfrę z kodu - koszty utrzymania różne w zależności od miejsca zamieszkania
        if (ileZostajeNaSplate.signum() == -1) {
//            req.setAttribute("msg", "Brak zdolności kredytowej z powodu niskich miesięcznych dochodów");
            kredytyLi.add("Brak zdolności kredytowej z powodu niskich miesięcznych dochodów. Dochód: "+dochod+" System wyliczył że na splate miesięcznie pozostanie tylko: "+ileZostajeNaSplate);
        } else {
            Integer ileMiesiecy = kwotaZakupu.divide(ileZostajeNaSplate, 0, RoundingMode.CEILING).intValue();
//            req.setAttribute("msg", "Po przeliczeniu klient powinien spłacać raty przez " + ileMiesiecy + " miesięcy w wysokości " + ileZostajeNaSplate.setScale(2, RoundingMode.UP));
            kredytyLi.add("Dochód: "+dochod+" Po przeliczeniu klient powinien spłacać raty przez " + ileMiesiecy + " miesięcy w wysokości " + ileZostajeNaSplate.setScale(2, RoundingMode.UP));
        }

        req.getSession().setAttribute("kredytyList", kredytyLi);

//        req.getRequestDispatcher("kredytResponse.jsp").forward(req, resp);
        req.getRequestDispatcher("kredytLista.jsp").forward(req, resp);

    }
}
